## Step 1: Activating the plugin

* Put the plugin in the correct directory: You will need to put the folder named `nerva` from this repo/unzipped release into the wordpress plugins directory. This can be found at `path/to/wordpress/folder/wp-content/plugins`

* Activate the plugin from the WordPress admin panel: Once you login to the admin panel in WordPress, click on "Installed Plugins" under "Plugins". Then simply click "Activate" where it says "Nerva - WooCommerce Gateway"

## Step 2 : Get a nerva daemon to connect to

### Running a full node

To do this: start the nerva daemon on your server and leave it running in the background. This can be accomplished by running `./nervad` (Linux) `nervad.exe` (Windows) inside your nerva downloads folder. The first time that you start your node, the nerva daemon will download and sync the entire nerva blockchain. This can take several hours and is best done on a machine with at least 4GB of ram, an SSD hard drive (with at least 5GB of free space), and a high speed internet connection.

### Setup your  nerva wallet-rpc

* Setup a nerva wallet using the nerva-wallet-cli tool.

* [Create a view-only wallet from that wallet for security.] Optional

    Linux
    
    `./nerva-wallet-cli --generate-from-view-key viewOnlyWalletFile` 
    
    Windows
    
    `nerva-wallet-cli.exe --generate-from-view-key viewOnlyWalletFile` 

* Start the Wallet RPC and leave it running in the background. This can be accomplished by running


    Linux
    
    `./nerva-wallet-rpc --rpc-bind-port 43929 --disable-rpc-login --log-level 2 --wallet-file /path/viewOnlyWalletFile --password password --daemon-address 127.0.0.1:17566` where "/path/viewOnlyWalletFile" is the wallet file for your view-only wallet.
   
    Windows
    
    `nerva-wallet-rpc.exe --rpc-bind-port 43929 --disable-rpc-login --log-level 2 --wallet-file /path/viewOnlyWalletFile --password password --daemon-address 127.0.0.1:17566` where "/path/viewOnlyWalletFile" is the wallet file for your view-only wallet.

## Step 4: Setup Nerva Gateway in WooCommerce

* Navigate to the "settings" panel in the WooCommerce widget in the WordPress admin panel.

* Click on "Checkout"

* Select "Nerva GateWay"

* Check the box labeled "Enable this payment gateway"

* Check "Use nerva-wallet-rpc"


* Enter your nerva wallet address in the box labled "Nerva Address". If you do not know your address, you can run the `address` commmand in your nerva wallet

* Enter the IP address of your server in the box labeled "Nerva wallet rpc Host/IP"

* Enter the port number of the Wallet RPC in the box labeled "Nerva wallet rpc port" (it would be `43929` if you used the above example).

Finally:

* Click on "Save changes"

Original source README.md was for masari, This is the edited version according to Nerva
